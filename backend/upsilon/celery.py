# noqa
# pylint: skip-file
from django_utils.celery.app import *

app.conf.beat_schedule["oauth_clear_tokens"] = {
    "task": "upsilon.tasks.clear_tokens",
    "schedule": 600.0,  # 10 minutes
}
