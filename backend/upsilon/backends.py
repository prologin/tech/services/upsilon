from django.conf import settings
from django.contrib.auth.backends import RemoteUserBackend
from django_auth_ldap.backend import LDAPBackend

from .utils import KerberosPrincipalStr, MalformedPrincipal


class RemoteUserLDAPBackend(RemoteUserBackend):
    def clean_username(self, username):
        try:
            krb_principal = KerberosPrincipalStr(username)
        except MalformedPrincipal:
            return None
        if krb_principal.realm != settings.KERBEROS_REALM:
            return None
        return krb_principal.primary

    def configure_user(self, request, user, created=True):
        return LDAPBackend().populate_user(user.username)
