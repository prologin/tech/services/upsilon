from django.urls import include, path
from django_utils.urls import default, drf

urlpatterns = (
    default.urlpatterns()
    + drf.urlpatterns()
    + [
        path(
            "o/", include("oauth2_provider.urls", namespace="oauth2_provider")
        ),
    ]
)
