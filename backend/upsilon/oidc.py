from oauth2_provider.oauth2_validators import OAuth2Validator


class OAuth2Validator(OAuth2Validator):
    def get_additional_claims(self, request):
        return {
            "preferred_username": request.user.username,
            "email": request.user.email,
        }
