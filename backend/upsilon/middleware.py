from django.contrib.auth.middleware import RemoteUserMiddleware


class RemoteUserMiddleware(RemoteUserMiddleware):
    header = "HTTP_X_REMOTE_USER"
