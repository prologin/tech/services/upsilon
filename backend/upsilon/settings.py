# noqa
# pylint: skip-file
"""
Django settings for upsilon project.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.0/ref/settings/
"""

from pathlib import Path

import ldap
from django_auth_ldap.config import GroupOfNamesType, LDAPSearch
from django_utils.settings.caches import *
from django_utils.settings.common import *
from django_utils.settings.databases import *
from django_utils.settings.logging import *

from django_utils.settings.celery import *  # isort:skip
from django_utils.settings.drf import *  # isort:skip


PROJECT_NAME = "upsilon"

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Application definition

INSTALLED_APPS = installed_apps() + [
    "oauth2_provider",
]

MIDDLEWARE = middleware() + [
    "upsilon.middleware.RemoteUserMiddleware",
]

DATABASES = databases(PROJECT_NAME)

ROOT_URLCONF = root_urlconf(PROJECT_NAME)

WSGI_APPLICATION = wsgi_application(PROJECT_NAME)

REST_FRAMEWORK = rest_framework()


AUTHENTICATION_BACKENDS = ("upsilon.backends.RemoteUserLDAPBackend",)


KERBEROS_REALM = env.get_string("KERBEROS_REALM")


AUTH_LDAP_SERVER_URI = env.get_string("AUTH_LDAP_SERVER_URI")
AUTH_LDAP_BIND_DN = env.get_string("AUTH_LDAP_BIND_DN")
AUTH_LDAP_BIND_PASSWORD = env.get_secret("AUTH_LDAP_BIND_PASSWORD")

AUTH_LDAP_USER_SEARCH = LDAPSearch(
    env.get_string("AUTH_LDAP_USER_SEARCH_BASE"),
    ldap.SCOPE_SUBTREE,
    env.get_string("AUTH_LDAP_USER_SEARCH_FILTER", "(uid=%(user)s)"),
)

AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
    env.get_string("AUTH_LDAP_GROUP_SEARCH_BASE"),
    ldap.SCOPE_SUBTREE,
    env.get_string(
        "AUTH_LDAP_GROUP_SEARCH_FILTER", "(objectClass=groupOfNames)"
    ),
)
AUTH_LDAP_GROUP_TYPE = GroupOfNamesType(
    name_attr=env.get_string("AUTH_LDAP_GROUP_NAME_ATTR", "cn")
)

AUTH_LDAP_USER_ATTR_MAP = {
    "first_name": "givenName",
    "last_name": "sn",
    "email": "mail",
}

AUTH_LDAP_ALWAYS_UPDATE_USER = True
AUTH_LDAP_CACHE_TIMEOUT = 3600


OAUTH2_PROVIDER = {
    "PKCE_REQUIRED": env.get_bool("OAUTH_PKCE_REQUIRED", False),
    "OIDC_ENABLED": True,
    "OIDC_RSA_PRIVATE_KEY": env.get_secret("OIDC_RSA_PRIVATE_KEY"),
    "OAUTH2_VALIDATOR_CLASS": "upsilon.oidc.OAuth2Validator",
    "SCOPES": {
        "openid": "OpenID Connect scope",
        "profile": "Basic user profile informations",
        "email": "User email address",
    },
}
