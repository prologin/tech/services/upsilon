# django upsilon

### To create a project from this upsilon

Ask a root to create a project for you!

```bash
project_name=<fill me>
mv upsilon $project_name
```

### Setup a development environment

Install `docker` and `docker-compose` using your distribution's documentation.

You will also need `poetry` and `pre-commit`. Those are not
strictly needed to develop on this project, but they will become very handy for
things like adding dependencies, or pre-commit hooks.

For those using Nix, everything is ready in the `flake.nix` file.

#### Initial setup

```sh
# You can ignore errors about psycopg not being installed because it's missing
# local dependencies. Those are installed in Docker and not needed on your
# local machine.
$ pushd backend && poetry install && popd
$ cd docker/
$ ./gen_secrets.sh
$ DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose -p upsilon up --build
```

#### Regular usage

```sh
$ cd docker/
$ DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose -p upsilon up --build
```

#### Formatting

We use `black` and `isort` as code formatters. They can be enabled
through a git pre-commit with the following command:

```sh
$ pre-commit install
```

#### Linting

We also use `pylint` (via `prospector`) to check for common Python errors. You
can use the following commands to check your code:

```sh
$ cd docker/
$ docker-compose -p upsilon exec backend_dev prospector --profile base
```

#### Services

##### Website

```
URL: http://localhost:8000/
To login with a user in your fixtures: http://localhost:8000/admin
To create a superuser:
$ cd docker && ./manage.sh createsuperuser
```

##### Adminer

```
URL: http://localhost:8010/
System: 'PostgreSQL'
User: 'upsilon_dev'
Password in: docker/secrets/postgres-passwd
Database: 'upsilon_dev'
```
